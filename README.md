# What's here
Repository dove raccolgo tutto il codice in preparazione del progetto d'esame per il corso di Artificial Intelligence for Astrophysical Problems (AIAP)

# Files

## Data preparation
- [00_dataPreparation.ipynb](./00_dataPreparation.ipynb): In questo quaderno vado ad effettuare una prima prova di caricamento del dataset, creo il vettore delle classi vere in quanto il dataset ne è sprovvisto e lo salvo in formato `npz`
- [01_dataExploration.ipynb](./01_dataExploration.ipynb): In questo quaderno vado ad esplorare il dataset, studiando la distribuzione dei ground truth e delle features. Vado anche a rimuovere eventuali eventi patologici, salvando il dataset definitivo da usare per le analisi
- [02_dataScalingPCA.ipynb](./02_dataScalingPCA.ipynb): In questo quaderno vado ad effettuare test di riscalamento e PCA, per poter effettuare un pruning sulle features

## Random Forest Classifier
- [10_RandomForest.py](./10_RandomForest.py): Effettuo una grid search su alcuni parametri relativi al [`RandomForestClassifier`](https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestClassifier.html) dopo aver effettuato PCA e pruning
- [11_anaRandomForest.ipynb](./11_anaRandomForest.ipynb): Analizzo le performance del classificatore
