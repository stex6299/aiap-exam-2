#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 20 08:30:14 2023

@author: steca
"""

# Allows debug on spyder in my spyder-cf environment
#import collections
#collections.Callable = collections.abc.Callable


#%% Import moduli
import numpy as np
from matplotlib import pyplot as plt
#import h5py

import time, os, re, sys

from sklearn import utils
from sklearn import model_selection 
from sklearn import preprocessing
from sklearn import metrics

from sklearn.model_selection import train_test_split, ParameterGrid

# K-fold validation
from sklearn.model_selection import RepeatedStratifiedKFold

# PCA
from sklearn.decomposition import PCA

# Scoring
# from sklearn.metrics import confusion_matrix, accuracy_score, precision_score, recall_score, f1_score
# from sklearn.metrics import roc_curve, auc
from sklearn import metrics

# Classificatori
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier



#%% Settings

# >>>
prefisso = "Adaboost_95"
resultFile = f"Results/{prefisso}.npz"

print(f"Benvenuto nel programma {prefisso}")
print(f"Gli output saranno salvati in {resultFile}")


# Se crasha, per riniziare da un punto civile
startGrid = 0
StartKF = 0

thrEV = .95     # Soglia Explained Variance per pruning componenti
drawPlot = False

if drawPlot:
    #%matplotlib inline
    pass
    
testEvts = None    # Numero di eventi del TeS da usare per il test (None = tutti)
n_jobs = -1        # Numero di processi paralleli (-1 = Numero di cores)

    
# K-fold
n_splits = 5    # Numero folder
n_repeats = 2   # Numero ripetizioni


# >>>
# Grid search
n_estimators = [1, 10, 50, 100, 150, 200, 300, 500, 600]    # quanti weak learners
learning_rate = [.01, .05, .1, .5, 1, 1.5, 5]                         # learning rate
algorithm = ["SAMME.R"]


# >>>
# Dizionario per la grid search
paramDict = {"n_estimators": n_estimators,
             "learning_rate": learning_rate,
             "algorithm": algorithm}




# ROC Curve
nrocstep = 1000                            # Numero punti per curva ROC
xROC = np.linspace(0,1,nrocstep)           # Vettore su cui interpolare curva ROC


#%% Load dei dati
#os.chdir(r"G:\Corsi\AIAP - Artificial Intelligence for Astrophysical Problems\Esame\Repo")
dati = np.load("Dataset/dati_puliti.npz")
#dati.shape

X = dati["X"]
y = dati["y"]

X, y = utils.shuffle(X, y)

#%% Verifico di aver shufflato [Copiato da 01]

# Indice degli eventi (0, 1, 2....)
timeVect = np.arange(0, X.shape[0], step = 1)

if drawPlot:
    fig, ax = plt.subplots()
    h = ax.hist2d(timeVect, y, cmap = "jet")
    
    ax.set_xlabel("Numero evento", fontsize = 14)
    ax.set_ylabel("Ground truth", fontsize = 14)
    
    ax.set_yticks((0,1))
    
    
    fig.colorbar(h[3], ax = ax)
    
    plt.show()


#%% Grid search

# Definisco la grid search
list_grid = list(ParameterGrid(paramDict))
nStepGrid = len(list_grid)
print(f"Saranno provate {nStepGrid} configurazioni di iper-parametri")





#%% Applico k-fold

"""
Faccio 5 folders in modo tale da usare il 20% come test set
Faccio 2 ripetizioni in modo tale da fare diei validazioni per ciascun 
dataset e per ciascuna configurazione di iper-parametri
"""
rskf = RepeatedStratifiedKFold(n_splits = n_splits, 
                               n_repeats = n_repeats, 
                               random_state = 13)

nStepKF = rskf.get_n_splits(X, y)
print(f"Saranno effettuati {nStepKF} step di k-fold per ciascuna configurazione di iper-parametri")



#%% Vettori globali di scoring

# Valori medi
globConfusionMatrix = np.zeros((2, 2, nStepGrid), dtype = float)

globAccuracy    = np.zeros(nStepGrid, dtype = float)
globPrecision   = np.zeros(nStepGrid, dtype = float)
globRecall      = np.zeros(nStepGrid, dtype = float)
globF1          = np.zeros(nStepGrid, dtype = float)

globxROC        = np.zeros((nrocstep, nStepGrid), dtype = float)
globyROC        = np.zeros((nrocstep, nStepGrid), dtype = float)
globAUC         = np.zeros(nStepGrid, dtype = float)




# Errori
globConfusionMatrixErr = np.zeros((2, 2, nStepGrid), dtype = float)

globAccuracyErr     = np.zeros(nStepGrid, dtype = float)
globPrecisionErr    = np.zeros(nStepGrid, dtype = float)
globRecallErr       = np.zeros(nStepGrid, dtype = float)
globF1Err           = np.zeros(nStepGrid, dtype = float)



globxROCErr         = np.zeros((nrocstep, nStepGrid), dtype = float)
globyROCErr         = np.zeros((nrocstep, nStepGrid), dtype = float)
globAUCErr          = np.zeros(nStepGrid, dtype = float)




# I valori ritornati da metrics.roc_curve li esporto come liste perché
# non capisco la logica con cui stabilisce quanti punti fare
globallstfpr = []
globallsttpr = []
globallstthresholds = []


# >>>
# Iper-parametri, li esporto anche come liste singole
iperParam_n_estimators      = []
iperParam_learning_rate     = []
iperParam_algorithm         = []


#%% Big loop

T = time.time()

# Grid search: Ciclo su una certa combinazione di iper parametri
for j, grid_dict in enumerate(list_grid):
    
    # Condizione per iniziare in ritardo. In realtà, per come salvo, è "inutile"
    if j < startGrid: continue

    
    
    # >>>
    # Estraggo i parametri dello step corrente grid search
    tmp_n_estimators = grid_dict["n_estimators"]
    tmp_learning_rate = grid_dict["learning_rate"]
    tmp_algorithm = grid_dict["algorithm"]

    
    # >>>
    # Li appendo a delle liste per poterli esportare
    iperParam_n_estimators.append(tmp_n_estimators)
    iperParam_learning_rate.append(tmp_learning_rate)
    iperParam_algorithm.append(tmp_algorithm)

    
    
    
    # Resetto i parametri per lo scoring che medierò sulle k-fold
    tmpConfusionMatrix = np.zeros((2, 2, nStepKF), dtype = float)
    
    tmpAccuracy     = np.zeros(nStepKF, dtype = float)
    tmpPrecision    = np.zeros(nStepKF, dtype = float)
    tmpRecall       = np.zeros(nStepKF, dtype = float)
    tmpF1           = np.zeros(nStepKF, dtype = float)
    
    
    
    # ROC Curves
    
    # Punti densi per calcolo aree
    tmpxROC = np.zeros((nrocstep, nStepKF), dtype = float)
    tmpyROC = np.zeros((nrocstep, nStepKF), dtype = float)
    
    # Esporto come liste anche i valori grezzi 
    # (liste perché non capisco che lunghezza hanno i vettori ritornati. 
    # Forse potrebbero essere non standard?)
    lstfpr = []
    lsttpr = []
    lstthresholds = []
    
    tmpAUC = np.zeros(nStepKF, dtype = float)
    
    
    
    
    # K-Fold: Itero su una certa combinazione di TrS/TeS
    for i, (train_index, test_index) in enumerate(rskf.split(X, y)):
        
        # Condizione per iniziare in ritardo. In realtà, per come salvo, è "inutile"
        if (j == startGrid) & (i < StartKF): continue
        
        print(f"\n\n\nStep grid Search: {j+1} / {nStepGrid}\tStep K-Fold: {i+1} / {nStepKF}")
        
        
        # Ottengo TrS e TeS usando le varie K-fold
        X_train = X[train_index,:]
        y_train = y[train_index]
        X_test = X[test_index,:]
        y_test = y[test_index]
        
        
        
        # *** Data Preparation ***
        
        # Applico scaling MinMax
        scaler = preprocessing.StandardScaler()
        scaler.fit(X_train)
        X_train_scaled = scaler.transform(X_train)


        # Applico la PCA
        pca = PCA()
        pca.fit(X_train_scaled)
        X_train_pca = pca.transform(X_train_scaled)


        # Estraggo le Explained Variances
        EV = pca.explained_variance_ratio_
        cumEV = np.cumsum(EV)


        # Stabilisco che componenti tenere per il pruning
        # Le componenti sono già ordinate
        idxLastFeature = np.sum(cumEV <= thrEV)
        print(f"Per avere il {thrEV * 100} % di Explaied Variance, sono necessarie {idxLastFeature} features ")


        # Pruno tenendo solo le prime componenti più informative
        X_train_pruned = X_train_pca[:,:idxLastFeature]     # Pruno




        # Estraggo info su Test Set
        X_test_scaled = scaler.transform(X_test)        # Riscalo
        X_test_pca = pca.transform(X_test_scaled)       # Trasformo PCA 
        X_test_pruned = X_test_pca[:,:idxLastFeature]   # Pruno



        # >>>
        # Definisco il classifier
        classifier = AdaBoostClassifier(n_estimators = tmp_n_estimators,
                                          learning_rate = tmp_learning_rate,
                                          algorithm = tmp_algorithm,
                                          random_state = 0)

        
        
        
        
        # *** Training ***
        
        t = time.time()
        print("\n-->Che il training abbia inizio...")
        classifier.fit(X_train_pruned, y_train)
        print(f"Time elapsed for training: {time.time()-t:.2f} s")
        
        
        
        
        # *** Prediction ***
        
        t = time.time()
        print("\n-->Che la predizione abbia inizio...")
        y_pred = classifier.predict(X_test_pruned[:testEvts,:])
        y_pred_proba = classifier.predict_proba(X_test_pruned[:testEvts,:])[:,1]
        print(f"Time elapsed for prediction: {time.time()-t:.2f} s")
        
        
        
        
        # *** Scoring ***
        
        confusionMatrix =   metrics.confusion_matrix(y_test[:testEvts], y_pred)
        accuracy        =   metrics.accuracy_score(y_test[:testEvts], y_pred)
        precision       =   metrics.precision_score(y_test[:testEvts], y_pred)
        recall          =   metrics.recall_score(y_test[:testEvts], y_pred)
        f1              =   metrics.f1_score(y_test[:testEvts], y_pred)
        
        
        
        # Salvo gli scoring parziali
        tmpConfusionMatrix[:,:,i] = confusionMatrix
        tmpAccuracy[i] = accuracy
        tmpPrecision[i] = precision
        tmpRecall[i] = recall
        tmpF1[i] = f1
        
        
        
        
        
        # *** ROC Curve ***
        fpr, tpr, thresholds = metrics.roc_curve(y_test, y_pred_proba)
        
        # Creo un'interpolazione denza per miglior calcolo aree
        yROC = np.interp(xROC, fpr, tpr)
        #myAuc = metrics.auc(fpr, tpr)
        myAUC = metrics.auc(xROC, yROC)
        
        
        # Salvo i valori
        tmpxROC[:,i] = xROC
        tmpyROC[:,i] = yROC
        
        
        # 
        lstfpr.append(fpr)
        lsttpr.append(tpr)
        lstthresholds.append(thresholds)
        
        
        
        tmpAUC[i] = myAUC
        
        
        
        
        
        
        # Some output
        print(f"\nResults:")
        print("Confusion Matrix: ")
        print(confusionMatrix)

        print(f"Print: Accuracy: {accuracy:.2f}")
        print(f"Print: precision: {precision:.2f}")
        print(f"Print: recall: {recall:.2f}")
        print(f"Print: f1: {f1:.2f}")

        print(f"Print: AUC: {myAUC:.2f}")


        
        

            
            
        # print(classifier.get_params())
        # print(grid_dict)
        
        
        
    # *** MEDIE SU K-FOLD ***

    # Medio le grandezze di scoring sulle varie k-fold        
    meanConfusionMatrix = np.mean(tmpConfusionMatrix, axis = 2)
    
    meanAccuracy = np.mean(tmpAccuracy)
    meanPrecision = np.mean(tmpPrecision)
    meanRecall = np.mean(tmpRecall)
    meanF1 = np.mean(tmpF1)
    
    
    # Medio le curve ROC
    meanxROC = np.mean(tmpxROC, axis = 1)
    meanyROC = np.mean(tmpyROC, axis = 1)
    meanAUC = np.mean(tmpAUC)
    
        
    
    # *** ERRORI SU K-FOLD ***
    
    # Scoring
    stdConfusionMatrix = np.std(tmpConfusionMatrix, axis = 2) / np.sqrt(nStepKF - 1)
    
    stdAccuracy = np.std(tmpAccuracy) / np.sqrt(nStepKF - 1)
    stdPrecision = np.std(tmpPrecision) / np.sqrt(nStepKF - 1)
    stdRecall = np.std(tmpRecall) / np.sqrt(nStepKF - 1)
    stdF1 = np.std(tmpF1) / np.sqrt(nStepKF - 1)
    
    
    # ROC
    stdxROC = np.std(tmpxROC, axis = 1) / np.sqrt(nStepKF - 1)
    stdyROC = np.std(tmpyROC, axis = 1) / np.sqrt(nStepKF - 1)
    stdAUC = np.std(tmpAUC) / np.sqrt(nStepKF - 1)
    
    
    
    # *** Appendo ai vettori globali ***
    
    # Medie
    globConfusionMatrix[:,:,j] = meanConfusionMatrix
    
    globAccuracy[j] = meanAccuracy
    globPrecision[j] = meanPrecision
    globRecall[j] = meanRecall
    globF1[j] = meanF1
    
    globxROC[:,j] = meanxROC
    globyROC[:,j] = meanyROC
    globAUC[j] = meanAUC
    
    
    # Err
    globConfusionMatrixErr[:,:,j] = stdConfusionMatrix
    
    globAccuracyErr[j] = stdAccuracy
    globPrecisionErr[j] = stdPrecision
    globRecallErr[j] = stdRecall
    globF1Err[j] = stdF1
    
    globxROCErr[:,j] = stdxROC
    globyROCErr[:,j] = stdyROC
    globAUCErr[j] = stdAUC
    
    
    
    
    # Esporto anche i valori grezzi dati dalle ROC curves, just in case
    globallstfpr.append(lstfpr)
    globallsttpr.append(lsttpr)
    globallstthresholds.append(lstthresholds)
    
    
    
    
    
    # ***** Salvataggio Files *****
    
    # Scrivo in un file di testo per comodità
    print("\nSto per dumpare nel file di testo rapido...")
    with open(resultFile.replace("npz", "summary.txt"), "a") as f:
        if j == 0:
            
            strToWrite = ""
            
            # >>>
            # Step e iper parametri
            strToWrite += "{Step}\t"
            strToWrite += "{tmp_n_estimators}\t"
            strToWrite += "{tmp_learning_rate}\t"
            strToWrite += "{tmp_algorithm}\t"

            # Global scoring
            strToWrite += "{meanConfusionMatrix}\t"
            strToWrite += "{meanAccuracy}\t"
            strToWrite += "{meanPrecision}\t"
            strToWrite += "{meanRecall}\t"
            strToWrite += "{meanF1}\t"
            strToWrite += "{meanAUC}\n" # Note final N
            
            
            f.write(strToWrite)

            #f.write("n_estimators\tcriterion\tmin_samples_leaf\tmax_depth\tmeanConfusionMatrix\tmeanAccuracy\tmeanPrecision\tmeanRecall\tmeanF1\tmeanAUC\n")
        #f.write(f"{tmp_n_estimators}\t{tmp_criterion}\t{tmp_min_samples_leaf}\t{tmp_max_depth}\t\t{meanAccuracy:.2f}\t{meanPrecision:.2f}\t{meanRecall:.2f}\t{meanF1:.2f}\t{meanAUC:.2f}\n")
        
        strToWrite = ""
        
        # >>>
        # Step e iper parametri
        strToWrite += f"{j+1}/{nStepGrid}\t"
        strToWrite += f"{tmp_n_estimators}\t"
        strToWrite += f"{tmp_learning_rate}\t"
        strToWrite += f"{tmp_algorithm}\t"

        # Global scoring
        strToWrite += f"{meanConfusionMatrix.reshape((1,4))}\t"
        strToWrite += f"{meanAccuracy:.2f}\t"
        strToWrite += f"{meanPrecision:.2f}\t"
        strToWrite += f"{meanRecall:.2f}\t"
        strToWrite += f"{meanF1:.2f}\t"
        strToWrite += f"{meanAUC:.2f}\n" # Note final N
        
        
        f.write(strToWrite)
    print("...Ho dumpato\n")
        
    
    
    
    
    
    
    print("\nSto per dumpare nel file di testo globale...")
    with open(resultFile.replace("npz", "complete.txt"), "a") as f:
        if j == 0:
            strToWrite = ""
            
            # >>>
            # Step e iper parametri
            strToWrite += "{Step}\t"
            strToWrite += "{tmp_n_estimators}\t"
            strToWrite += "{tmp_learning_rate}\t"
            strToWrite += "{tmp_algorithm}\t"

            
            # Global scoring
            strToWrite += "{meanConfusionMatrix.reshape((1,4))}\t"
            strToWrite += "{meanAccuracy}\t"
            strToWrite += "{meanPrecision}\t"
            strToWrite += "{meanRecall}\t"
            strToWrite += "{meanF1}\t"
            strToWrite += "{meanAUC}\t"
            
            # Global scoring err
            strToWrite += "{stdConfusionMatrix.reshape((1,4))}\t"
            strToWrite += "{stdAccuracy}\t"
            strToWrite += "{stdPrecision}\t"
            strToWrite += "{stdRecall}\t"
            strToWrite += "{stdF1}\t"
            strToWrite += "{stdAUC}\n" # Note final N


            f.write(strToWrite)
        
        
        
        strToWrite = ""
        
        # >>>
        # Step e iper parametri
        strToWrite += f"{j+1}/{nStepGrid}\t"
        strToWrite += f"{tmp_n_estimators}\t"
        strToWrite += f"{tmp_learning_rate}\t"
        strToWrite += f"{tmp_algorithm}\t"

        
        # Global scoring
        strToWrite += f"{meanConfusionMatrix.reshape((1,4))}\t"
        strToWrite += f"{meanAccuracy}\t"
        strToWrite += f"{meanPrecision}\t"
        strToWrite += f"{meanRecall}\t"
        strToWrite += f"{meanF1}\t"
        strToWrite += f"{meanAUC}\t"
        
        # Global scoring err
        strToWrite += f"{stdConfusionMatrix.reshape((1,4))}\t"
        strToWrite += f"{stdAccuracy}\t"
        strToWrite += f"{stdPrecision}\t"
        strToWrite += f"{stdRecall}\t"
        strToWrite += f"{stdF1}\t"
        strToWrite += f"{stdAUC}\n" # Note final N
        
        
        f.write(strToWrite)
    print("...Ho dumpato\n")
        
    
    
    
    
    print("\nSto per dumpare nel file di testo delle roc curves...")
    with open(resultFile.replace("npz", "roc.txt"), "a") as f:
        strToWrite = ""
        
        # Step e iper parametri
        strToWrite += f"Step: {j+1}/{nStepGrid}\n"
        strToWrite += f"{meanxROC}\n"
        strToWrite += f"{meanyROC}\n"
        strToWrite += f"{stdxROC}\n"
        strToWrite += f"{stdyROC}\n"
        
        f.write(strToWrite)
    print("...Ho dumpato\n")
        



print(f"\n\nFine del big loop. Time elapsed {time.time()-T:.2f} s")


#%% Save data
print(f"Sto per salvare {resultFile}")

# Salvo le grandezze mediate sulle ripetizioni dei Kfold, con rispettivi errori
np.savez(resultFile, globConfusionMatrix = globConfusionMatrix,
                     globAccuracy = globAccuracy,
                     globPrecision = globPrecision,
                     globRecall = globRecall,
                     globF1 = globF1,
                     
                     globxROC = globxROC,
                     globyROC = globyROC,
                     globAUC = globAUC,
                     
                     globConfusionMatrixErr = globConfusionMatrixErr,
                     globAccuracyErr = globAccuracyErr,
                     globPrecisionErr = globPrecisionErr,
                     globRecallErr = globRecallErr,
                     globF1Err = globF1Err,
                     
                     globxROCErr = globxROCErr,
                     globyROCErr = globyROCErr,
                     globAUCErr = globAUCErr,
                     
                     
                     globallstfpr = np.array(globallstfpr, dtype = object),
                     globallsttpr = np.array(globallsttpr, dtype = object),
                     globallstthresholds = np.array(globallstthresholds, dtype = object),
                     
                     # >>>
                     list_grid = list_grid,
                     iperParam_n_estimators = iperParam_n_estimators,
                     iperParam_learning_rate = iperParam_learning_rate, 
                     iperParam_algorithm = iperParam_algorithm
                     )

print(f"Ho salvato {resultFile}")
print("Bye bye")
